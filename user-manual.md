#### MENA RATES VERSION 1.0

* MENA Rates is a website helps the merchant in MENA to get the information he needs about the online gateways.

* As a store administrator you can add, edit and delete the information from the admin panel without need to have a background programing.

* To access the backend go to **mena-rates/admin** using the following credential :
    * Username : admin
    * Password : admin

* On click **Add Countries** you can be able to add countries, so you can link them with gateways later.

* The same for **Add Currencies, Add Payment Cards**.

* On **Manage Gateways** you can add , edit and delete online gateways.
    * To add new gateways you need to insert the gateway name.
    * link the countries, currencies and payment cards that the gateways supported.
    * Then click on save button.
    * A link will appear to add the rates for countries.

* To update the logo go the folder **mena-rates/includes/media** delete the existed images and upload your logo, and make sure to rename it to **img** with the extension **.png** and the **dimensions (195x130) px**.

* As a visitor you can use the side menu to filter the gateways based on your requirement.

* To get more information about each gateway you need to click on the gateway title a new menu will slide down contains more information.

* Also you can compare between two gateways, just you need to click on the other gateway tilte and you can know the diffenerence btween them.