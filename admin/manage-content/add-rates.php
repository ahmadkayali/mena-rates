<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>Add Rates for <?php echo get_item($connection); ?></h2>
	<?php 
		$result=get_available_countries_for_gateway($_GET["id"], $connection);
		echo $result[0];

		if ( isset($_POST["submit"]) ) 
		{
			for ($i=1; $i <count($result) ; $i++) 
			{ 
				$error=validation_rates($_POST["setup-{$result[$i]}"],$_POST["transaction-{$result[$i]}"]);
				if ($error) { echo "All fields are required and should be numeric."; break; }
			}

			if ($error==0) 
			{
				for ($i=1; $i <count($result) ; $i++) 
				{ 
					$exist= check_if_rates_exist($_POST["setup-{$result[$i]}"], $_POST["transaction-{$result[$i]}"], $connection);
					
					if ($exist=="not exist") 
					{
						insert_rate($_POST["setup-{$result[$i]}"],$_POST["transaction-{$result[$i]}"], $connection);
					}

					link_rates_with_gateway_and_country($exist, $result[$i], $connection);
				}

				echo "&nbsp; &nbsp; &nbsp; Rates has been added. ";
			}
		}
	?>
</div>

<?php 	require('../../includes/footer.php'); ?>
