<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<?php
	$result=display_rates_values_for_countries($_GET["id"], $connection);
	if ( isset($_POST["submit"]) ) 
	{
		for ($i=1; $i <count($result) ; $i++) 
		{ 
			$error=validation_rates($_POST["setup-{$result[$i]}"],$_POST["transaction-{$result[$i]}"]);
			if ($error) { $message= "All fields are required and should be numeric."; break; }
		}

		if ($error==0) 
		{
			for ($i=1; $i <count($result) ; $i++) 
			{ 
				$exist= check_if_rates_exist($_POST["setup-{$result[$i]}"], $_POST["transaction-{$result[$i]}"], $connection);

				if ($exist=="not exist") 
				{
					insert_rate($_POST["setup-{$result[$i]}"],$_POST["transaction-{$result[$i]}"], $connection);
				}

				$linked=check_if_rates_linked_with_countries($result[$i], $connection);
				if ($linked) 
				{
					update_rates_id_with_gateway_and_country($exist, $result[$i], $connection);
				}
				else
				{
					link_rates_with_gateway_and_country($exist, $result[$i], $connection);
				}	
			}

			delete_unused_rates($connection);
			$message="&nbsp; &nbsp; &nbsp; Rates has been added. ";
			$result_updated=display_rates_values_for_countries($_GET["id"], $connection);
		}
	}
?>

<div class="content">
	<h2>Edit Rates for <?php echo get_item($connection); ?></h2>
	
	<?php 
		if ( isset($_POST["submit"]) && $message=="&nbsp; &nbsp; &nbsp; Rates has been added. ") 
		{
			echo $result_updated[0];
		}
		else
		{
			echo $result[0];
		}

		if ( isset($_POST["submit"]) ) 
		{
			echo $message;
		}
	
	?>
</div>

<?php 	require('../../includes/footer.php'); ?>
