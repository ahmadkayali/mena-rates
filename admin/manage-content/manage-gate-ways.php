<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>Manage gateways</h2>
	<?php
		echo display_gateway_content($_GET["type"], $connection);
	?>
	<a class="button" href="add-new-gate-ways.php?type=gateways">Click to add a new gateway</a>
</div>

<?php 	require('../../includes/footer.php'); ?>
