<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>Welcome to Admin Panel.</h2>
	<?php 
		delete_gateway($connection);
	?>
</div>

<?php 	require('../../includes/footer.php'); ?>
