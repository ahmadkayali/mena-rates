<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<?php
	$old_value=get_item($connection);
	if ( isset($_POST["submit"]) )
	{
		$reslut=validation_error_on_item($_POST[$_GET["type"]], $connection);
		if ($reslut==1) 
		{
			$reslut=update_existing_item($_POST[$_GET["type"]], $connection);
			$new_value=get_item($connection); 
		}
	}
?>
	

<div class="content">
	<h2>Edit the <?php echo $_GET["type"];?>:</h2>
	<form action="edit-item.php?id=<?php echo $_GET["id"];?>&amp;type=<?php echo $_GET["type"];?>" method="post">
		<label>Item name:</label>
		<br>
		<input type="text" name="<?php echo $_GET["type"];?>" value="<?php if ( isset($_POST["submit"]) && $reslut=="<br>Item has been updated. " ) {echo $new_value;} else {echo $old_value;} ?>">
		<br><br>
		<input type="submit" name="submit" value="save">
		<br><br>
		<?php
			if ( isset($_POST["submit"]) )
			{
				echo $reslut;
			}
		?>
	</form>
</div>

<?php 	require('../../includes/footer.php'); ?>
