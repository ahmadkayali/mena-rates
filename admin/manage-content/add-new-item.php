<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>Add a new <?php echo $_GET["type"];?>:</h2>
	<form action="add-new-item.php?type=<?php echo $_GET["type"];?>" method="post">
		<label>Item name:</label>
		<br>
		<input type="text" name="<?php echo $_GET["type"];?>">
		<br><br>
		<input type="submit" name="submit" value="save">
		<br><br>
		<?php
			if ( isset($_POST["submit"]) )
			{
				$result=validation_error_on_item($_POST[$_GET["type"]], $connection);
				if ($result==1) 
				{
					add_new_item($_POST[$_GET["type"]], $connection);
					echo "Item has been added.";
				}
				else {echo $result;}
			}
		?>
	</form>
</div>

<?php 	require('../../includes/footer.php'); ?>
