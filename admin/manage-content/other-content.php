<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<div class="content">
	<h2>List of available <?php echo $_GET["type"];?>.</h2>
	<?php
		display_content($_GET["type"], $connection); 
	?>
	<a class="button" href="add-new-item.php?type=<?php echo $_GET["type"];?>">Click to add a new <?php echo $_GET["type"];?></a>

</div>

<?php 	require('../../includes/footer.php'); ?>
