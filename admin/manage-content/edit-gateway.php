<?php
	require('../../includes/functions.php');
	require('../../includes/db_connection.php');
	require('../../includes/sessions.php');
	check_if_loggedin();

?>

<?php 	require('../../includes/header.php'); ?>

<?php
	$old_value=get_item($connection);
	$option_country=dispaly_form_edit_content("countries", "gate_country", $connection);
	$option_currency=dispaly_form_edit_content("currencies", "gate_currency", $connection);
	$option_card=dispaly_form_edit_content("cards", "gate_card", $connection); 
	if ( isset($_POST["submit"]) ) 
	{

		if ( empty($_POST["gateway"]) ) 
		{
			$message= "item name is required";
		}
		elseif ( Record_Is_Exist("id", $_POST["gateway"], $connection) && Record_Is_Exist("id", $_POST["gateway"], $connection)!=$_GET["id"]) 
		{
			$message= "This name is already used.";
		}
		else
		{
			$result=array(0,0,0);
			$result_country=array();
			$result_currency=array();
			$result_card=array();
			
			$j=0;
			for ($i=1; $i<count($option_country); $i=$i+2) 
			{ 
				if ( isset($_POST[$option_country[$i]]) ) 
				{
					$result[0]=1;
					$result_country[$j]=$option_country[$i+1];
					$j=$j+1;
				}
			}

			$j=0;
			for ($i=1; $i<count($option_currency); $i=$i+2) 
			{ 
				if ( isset($_POST[$option_currency[$i]]) ) 
				{
					$result[1]=1;
					$result_currency[$j]=$option_currency[$i+1];
					$j=$j+1;
				}
			}

			$j=0;
			for ($i=1; $i<count($option_card); $i=$i+2) 
			{ 
				if ( isset($_POST[$option_card[$i]]) ) 
				{
					$result[2]=1;
					$result_card[$j]=$option_card[$i+1];
					$j=$j+1;
				}
			}

			$sum=0;
			for ($i=0; $i <=2 ; $i++) 
			{
				$sum = $sum+$result[$i]; 
			}
			if ($sum < 3) 
			{ $message= "Please select at least one option from each field."; }
		
			else
			{
				$countries_id=get_all_linked_countries_id($connection);
				delete_country_rates($result_country, $countries_id, $connection);
				$message=update_existing_item($_POST["gateway"], $connection);
				$new_value=get_item($connection); 
				delete_old_items("gate_country", $connection);
				delete_old_items("gate_currency", $connection);
				delete_old_items("gate_card", $connection);
				link_items_with_gateway($_GET["id"], "gate_country", $result_country, $connection);
				link_items_with_gateway($_GET["id"], "gate_currency", $result_currency, $connection);
				link_items_with_gateway($_GET["id"], "gate_card", $result_card, $connection);
				$option_country_updated=dispaly_form_edit_content("countries", "gate_country", $connection);
				$option_currency_updated=dispaly_form_edit_content("currencies", "gate_currency", $connection);
				$option_card_updated=dispaly_form_edit_content("cards", "gate_card", $connection);						
			}
		}
	}
?>

<div class="content">
	<h2>Edit <?php echo get_item($connection);?></h2>
	<form action="edit-gateway.php?id=<?php echo $_GET["id"]?>&amp;type=<?php echo $_GET["type"]?>" method="post">
		<label>Gateway name:</label>
		<br>
		<input type="text" name="gateway" value="<?php if ( isset($_POST["submit"]) && $message=="<br>Item has been updated. " ) {echo $new_value;} else {echo $old_value;} ?>">
		<br><br>

		<div class="side-input">
			<label>Available countries:</label>
			<br>
			<?php
				if ( isset($_POST["submit"]) && $message=="<br>Item has been updated. ")
				{
					echo $option_country_updated[0];
				}
				else
				{
					echo $option_country[0];
				}
			?>
			<br>
		</div>

		<div class="side-input">
			<label>Available currencies:</label>
			<br>
			<?php
				
				if ( isset($_POST["submit"]) && $message=="<br>Item has been updated. " )
				{
					echo $option_currency_updated[0];
				}
				else
				{
					echo $option_currency[0];
				}
			?>
			<br>
		</div>

		<div class="side-input">
			<label>Available cards:</label>
			<br>
			<?php
				
				if ( isset($_POST["submit"]) && $message=="<br>Item has been updated. ")
				{
					echo $option_card_updated[0];
				}
				else
				{
					echo $option_card[0];
				}
			?>
			<br>
		</div>
		
		<input style="display:block;" type="submit" name="submit" value="Save">
		<br>

		<?php
		
			$output="<a href=\"edit-rates.php?id=";
			$output.=$_GET["id"];
			$output.="&type=".$_GET["type"]."\">Click to edit rates</a><br>";
			echo $output;
			if ( isset($_POST["submit"]) ) 
			{
				echo $message;
			}

		?>
	</form>

</div>

<?php 	require('../../includes/footer.php'); ?>
