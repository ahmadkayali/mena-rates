<?php
	require('../includes/functions.php');
	require('../includes/db_connection.php');
	require('../includes/sessions.php');

?>


<DOCTYPE! html>
<html>
	<head>
		<title>Login Page:</title>
		<link rel="stylesheet" type="text/css" href="../includes/style/style.css">
	</head>
	<body>
		<div class="log-in">
			<h2>Please login:</h2>
			<br/>
			<form action="index.php" method="post">
				<label>User name:</label>
				<br/>
				<input type="text" name="username">
				<br/><br/>
				<label>Password:</label>
				<br/>
				<input type="password" name="password">
				<br/><br/>
				<input type="submit" name="submit" value="Login">
				<br/><br/>
				<?php
				if ( isset($_POST["submit"]) )
				{
					$log_result=check_user_credential($_POST["username"], $_POST["password"], $connection);
					if ($log_result) 
					{
						$_SESSION["login"]=1;
						redirect_to('manage-content');
						echo $log_result;
					}
					else {echo "Wrong Credential";}
				} 
				?>
			</form>
		</div>
	</body>
</html>

<?php 
	mysqli_close($connection);
?>