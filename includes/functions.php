<?php
	function redirect_to($location)
	{
		header("Location: $location");
	}

	function validation_error_on_item($item, $connection)
	{
		if ( empty($item)) 
		{
			return "Please fill all fields";
		}
		elseif ( strtoupper(Record_Is_Exist("name", $item, $connection)) == strtoupper($item)) 
		{
			return "This item is already exist.";
		}
		else
		{
			return 1;
		}
	}

	function add_new_item($item, $connection)
	{
		$query="INSERT INTO {$_GET["type"]}(name) ";
		$query.="VALUES(?) ";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'s', $item);
		mysqli_stmt_execute($stmt);
	}

	function update_existing_item($item, $connection)
	{
		$query="UPDATE {$_GET["type"]} ";
		$query.="SET name=? ";
		$query.="WHERE id={$_GET["id"]} ";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'s', $item);
		mysqli_stmt_execute($stmt);
		return "<br>Item has been updated. ";
	}

	function get_item($connection)
	{
		$query="SELECT name ";
		$query.="FROM {$_GET["type"]} ";
		$query.="WHERE id={$_GET["id"]} ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		mysqli_free_result($record);
		return $result["name"];
	}
	
	function Record_Is_Exist($data, $record, $connection)
	{
		$query="SELECT {$data} ";
		$query.="FROM {$_GET["type"]} ";
		$query.="WHERE name=?";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'s', $record);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $result);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		return $result;
	}

	function dispaly_form_content($content, $connection)
	{
		$query="SELECT id, name ";
		$query.="FROM {$content}";
		$result=mysqli_query($connection, $query);
		$content=array();
		$content[0]="";
		$count=0;
		while ($row=mysqli_fetch_assoc($result)) 
		{
			$content[0].="<input type=\"checkbox\" name=\"".$row["name"]."\">".$row["name"]."<br>";
			$count=$count+1;
			$content[$count]=$row["name"];
			$count=$count+1;
			$content[$count]=$row["id"];
		}
		mysqli_free_result($result);
		return $content;
	}


	function display_content($content, $connection)
	{
		$query="SELECT id, name ";
		$query.="FROM {$content}";
		$result=mysqli_query($connection, $query);
		echo "<ul>";
		while ($row=mysqli_fetch_assoc($result)) 
		{
			echo "<li>".$row["name"]."<a class=\"tab-spaces\" href=\"edit-item.php?id=".$row["id"]."&type=".$_GET["type"]."\">Edit</a></li>";
		}
		mysqli_free_result($result);
		echo "<br><br>";
	}


	function display_gateway_content($content, $connection)
	{
		$query="SELECT id, name ";
		$query.="FROM {$content}";
		$result=mysqli_query($connection, $query);
		$output= "<ul>";
		while ($row=mysqli_fetch_assoc($result)) 
		{
			$output.="<li>".$row["name"];
			$output.="<a class=\"tab-spaces\" href=\"delete-gateway.php?id=".$row["id"]."&type=".$_GET["type"]."\">Delete</a>";
			$output.="<a class=\"tab-space\" href=\"edit-gateway.php?id=".$row["id"]."&type=".$_GET["type"]."\">Edit</a>";

		}
		$output.= "<br><br>";
		mysqli_free_result($result);
		return $output;
	}

	function check_user_credential($username, $password, $connection)
	{
		$query="SELECT username, password ";
		$query.="FROM users ";
		$query.="WHERE username=?";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'s', $username);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $username_compare, $hash_password);
		mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		if (strcmp($username_compare, $username)) 
		{
			return 0;
		}
		else
			{
				$decode_password = crypt($password, $hash_password);
				if ($decode_password == $hash_password) 
				{
					return 1;
				}
				else
				{
					return 0;
				}
				
			}
	}

	function link_items_with_gateway($id, $table, $items, $connection)
	{
		for ($i=0; $i<count($items) ; $i++) 
		{ 
			$query="INSERT INTO {$table}(option_id, gate_id) ";
			$query.="VALUES ({$items[$i]}, {$id})";
			mysqli_query($connection, $query);
		}
	}

	function get_available_countries_for_gateway($id, $connection)
	{
		$query="SELECT name ";
		$query.="FROM countries ";
		$query.="JOIN gate_country ";
		$query.="on countries.id=gate_country.option_id ";
		$query.="WHERE gate_country.gate_id={$id} ";
		$record=mysqli_query($connection, $query);
		$output=array();
		$count=1;
		$output[0]= "<form action=\"add-rates.php?id=";
		$output[0].= $_GET["id"];
		$output[0].= "&type=";
		$output[0].= $_GET["type"];
		$output[0].= "\" method=\"post\">";
		while ( $row=mysqli_fetch_assoc($record) ) 
		{
			$output[0].="<div class=\"main-block\">";
			$output[0].="<h3>Rates for ".$row["name"]."</h3>";
			$output[0].="<div class=\"block\">";
			$output[0].= "<label>Setup Fee:</label><br>";
			$output[0].= "<input type=\"text\" name=\"setup-".$row["name"]."\">";
			$output[0].="</div>";
			$output[0].="<div class=\"block\">";
			$output[0].= "<label>Transaction Fee:</label><br>";
			$output[0].= "<input type=\"text\" name=\"transaction-".$row["name"]."\">";
			$output[0].="</div>";
			$output[0].="</div>";
			$output[$count]=$row["name"];
			$count=$count+1;

		}
		$output[0].="<input class=\"save-button\" type=\"submit\" name=\"submit\" value=\"Save\">";
		$output[0].="</form>";
		mysqli_free_result($record);
		return $output;
	}
	
	function validation_rates($setup, $transaction)
	{
		if ( empty($setup) || empty($transaction)) 
		{
			return 1;
		}
		elseif (!is_numeric($setup) || !is_numeric($transaction)) 
		{
			return 1;
		}
		else {return 0;}
	}

	function check_if_rates_exist($setupfee, $transactionfee, $connection)
	{
		$query= "SELECT id, gate_id ";
		$query.= "FROM rates ";
		$query.= "WHERE setupfee=? AND transactionfee=? ";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'dd', $setupfee, $transactionfee);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_bind_result($stmt, $id, $gate_id);
		$result=mysqli_stmt_fetch($stmt);
		mysqli_stmt_close($stmt);
		if ( $gate_id == $_GET["id"] ) 
		{
			return $id;
		}
		else { return "not exist"; }
	}

	function insert_rate($setupfee, $transactionfee, $connection)
	{
		$query= "INSERT INTO rates(setupfee, transactionfee, gate_id) ";
		$query.= "VALUES (?,?,?)";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'ddi', $setupfee, $transactionfee, $_GET["id"]);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
	}

	function link_rates_with_gateway_and_country($rate_id, $country_name, $connection)
	{
		if ($rate_id == "not exist") 
		{
			$rate_id=get_last_record("rates", $connection);
		}

		$id=get_country_id($country_name, $connection);
		$query="INSERT INTO rates_gateways_countries (rate_id, gate_id, country_id) ";
		$query.="VALUES ({$rate_id}, {$_GET["id"]}, {$id}) ";
		mysqli_query($connection, $query);
	}

	function get_last_record($table, $connection)
	{
		$query= "SELECT id FROM {$table} ORDER BY id desc LIMIT 1";
		$record= mysqli_query($connection, $query);
		$result= mysqli_fetch_assoc($record);
		mysqli_free_result($record);
		return $result["id"];
	}

	function dispaly_form_edit_content($content, $data_content, $connection)
	{
		$query="SELECT id, name ";
		$query.="FROM {$content}";
		$result=mysqli_query($connection, $query);
		$content=array();
		$content[0]="";
		$count=0;
		while ($row=mysqli_fetch_assoc($result)) 
		{
			$on=Is_Checked($row["id"], $_GET["id"], $data_content, $connection);
			$content[0].="<input type=\"checkbox\" name=\"".$row["name"]."\" ".$on.">".$row["name"]."<br>";
			$count=$count+1;
			$content[$count]=$row["name"];
			$count=$count+1;
			$content[$count]=$row["id"];
		}
		mysqli_free_result($result);
		return $content;
	}

	function Is_Checked($option_id, $gate_id, $table, $connection)
	{
		$query="SELECT id ";
		$query.="FROM {$table} ";
		$query.="WHERE gate_id={$gate_id} AND option_id={$option_id} ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		mysqli_free_result($record);
		if (!$result["id"]) { return ""; }
		else { return "checked"; }
	}

	function delete_old_items($table, $connection)
	{
		$query="DELETE FROM {$table} ";
		$query.="WHERE gate_id={$_GET["id"]}";
		mysqli_query($connection, $query);
	}

	function display_rates_values_for_countries($id, $connection)
	{
		$query="SELECT name ";
		$query.="FROM countries ";
		$query.="JOIN gate_country ";
		$query.="on countries.id=gate_country.option_id ";
		$query.="WHERE gate_country.gate_id={$id} ";
		$record=mysqli_query($connection, $query);
		$output=array();
		$count=1;
		$output[0]= "<form action=\"edit-rates.php?id=";
		$output[0].= $_GET["id"];
		$output[0].= "&type=";
		$output[0].= $_GET["type"];
		$output[0].= "\" method=\"post\">";
		while ( $row=mysqli_fetch_assoc($record) ) 
		{
			$output[$count]=$row["name"];
			$output[0].="<div class=\"main-block\">";
			$output[0].="<h3>Rates for ".$row["name"]."</h3>";
			$output[0].="<div class=\"block\">";
			$output[0].= "<label>Setup Fee:</label><br>";
			$output[0].= "<input type=\"text\" name=\"setup-".$row["name"]."\" value=";
			$output[0].=get_rates( $output[$count], "setupfee", $connection);
			$output[0].=">";
			$output[0].="</div>";
			$output[0].="<div class=\"block\">";
			$output[0].= "<label>Transaction Fee:</label><br>";
			$output[0].= "<input type=\"text\" name=\"transaction-".$row["name"]."\" value=";
			$output[0].=get_rates( $output[$count], "transactionfee", $connection);
			$output[0].=">";
			$output[0].="</div>";
			$output[0].="</div>";
			$count=$count+1;

		}
		$output[0].="<input class=\"save-button\" type=\"submit\" name=\"submit\" value=\"Save\">";
		$output[0].="</form>";
		mysqli_free_result($record);
		return $output;
	}

	function get_rates($country_name, $type, $connection)
	{
		$id=get_country_id($country_name, $connection);
		$query="SELECT {$type} ";
		$query.="FROM rates ";
		$query.="JOIN rates_gateways_countries ";
		$query.="ON rates.id=rates_gateways_countries.rate_id ";
		$query.="WHERE (rates_gateways_countries.country_id={$id} AND rates_gateways_countries.gate_id={$_GET["id"]}) ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		mysqli_free_result($record);
		return $result["{$type}"];

	}

	function update_rates($country_name, $setupfee, $transactionfee , $connection)
	{
		$country_id=get_country_id($country_name, $connection);
		$query="SELECT rate_id ";
		$query.="FROM  rates_gateways_countries ";
		$query.="WHERE (country_id={$country_id} AND gate_id={$_GET["id"]}) ";
		$record=mysqli_query($connection, $query);
		$rate=mysqli_fetch_assoc($record);
		$id=$rate["rate_id"];
		mysqli_free_result($record);

		$query="UPDATE rates ";
		$query.="SET setupfee=? , transactionfee=? ";
		$query.="WHERE id=$id ";
		$stmt=mysqli_prepare($connection, $query);
		mysqli_stmt_bind_param($stmt,'dd', $setupfee, $transactionfee);
		mysqli_stmt_execute($stmt);
		mysqli_stmt_close($stmt);
	}

	function check_rate_gate_country_linked($country_name, $connection)
	{
		$id=get_country_id($country_name, $connection);
		$query="SELECT rate_id ";
		$query.="FROM rates_gateways_countries ";
		$query.="WHERE (country_id={$id} AND gate_id={$_GET["id"]}) ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		if (!$result) 
		{
			return "not exist";
		}
		else
		{
			return $result["rate_id"];
		}

	}

	function get_country_id($country_name, $connection)
	{
		$query="SELECT id ";
		$query.="FROM countries ";
		$query.="WHERE name='{$country_name}' ";
		$record=mysqli_query($connection, $query);
		$country=mysqli_fetch_assoc($record);
		$id=$country["id"];
		mysqli_free_result($record);
		return $id;
	}

	function delete_gateway($connection)
	{
		$table=array("gate_card","gate_country","gate_currency","rates_gateways_countries","rates");
		for ($i=0; $i <count($table) ; $i++) 
		{ 
			$query="DELETE FROM {$table[$i]} ";
			$query.="WHERE gate_id={$_GET["id"]} ";
			mysqli_query($connection, $query);

		}

		$query="DELETE FROM gateways ";
		$query.="WHERE id={$_GET["id"]} ";
		mysqli_query($connection, $query);
		echo  "&nbsp; &nbsp; &nbsp; Gateway has been deleted. ";
	}

	function get_all_linked_countries_id($connection)
	{
		$query="SELECT option_id ";
		$query.="FROM gate_country ";
		$query.="WHERE gate_id={$_GET["id"]} ";
		$record=mysqli_query($connection, $query);
		$i=0;
		$id=array();
		while ( $result=mysqli_fetch_assoc($record) ) 
		{
			$id[$i]=$result["option_id"];
			$i=$i+1;
		}
		mysqli_free_result($record);
		return $id;
	}

	function delete_country_rates($new_ids, $old_ids, $connection)
	{
		for ($i=0; $i < count($old_ids) ; $i++) 
		{
			$flag=1;
			for ($j=0; $j < count($new_ids) ; $j++) 
			{ 
				if ($old_ids[$i]==$new_ids[$j]) 
				{
					$flag=0;
				}
			}
			if ($flag) 
			{
				$query="DELETE FROM rates_gateways_countries ";
				$query.="WHERE (country_id={$old_ids[$i]} AND gate_id={$_GET["id"]}) ";
				mysqli_query($connection, $query);
			}
		}
	}

	function check_if_rates_linked_with_countries($country_name, $connection)
	{
		$id=get_country_id($country_name, $connection);
		$query="SELECT rate_id ";
		$query.="FROM rates_gateways_countries ";
		$query.="WHERE (country_id={$id} AND gate_id={$_GET["id"]}) ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		if ($result) 
		{
			return 1;
		}
		else
		{
			return 0;
		}

	}

	function update_rates_id_with_gateway_and_country($rate_id, $country_name, $connection)
	{
		if ($rate_id == "not exist") 
		{
			$rate_id=get_last_record("rates", $connection);
		}

		$id=get_country_id($country_name, $connection);
		$query="UPDATE rates_gateways_countries ";
		$query.="SET rate_id={$rate_id} ";
		$query.="WHERE gate_id={$_GET["id"]} AND country_id={$id} ";
		mysqli_query($connection, $query);
	}

	function delete_unused_rates($connection)
	{
		$query="SELECT id ";
		$query.="FROM rates ";
		$query.="WHERE gate_id={$_GET["id"]}";
		$record=mysqli_query($connection, $query);
		while ( $result=mysqli_fetch_assoc($record) ) 
		{
			$query="SELECT id ";
			$query.="FROM rates_gateways_countries ";
			$query.="WHERE rate_id={$result["id"]} AND  gate_id={$_GET["id"]} ";
			$get_id=mysqli_query($connection, $query);
			$exist=mysqli_fetch_assoc($get_id);
			if ($exist) 
			{
				$test=1;
			}
			else
			{
				$query="DELETE FROM rates ";
				$query.="WHERE id={$result["id"]} ";
				mysqli_query($connection, $query);
			}
			mysqli_free_result($get_id);
		}

		mysqli_free_result($record);
	}


//--------------------------------------------------------	
// functions for retrieving records to front-end:


	function get_gateways($connection)
	{
		$query="SELECT name ";
		$query.="FROM gateways ";
		$result=mysqli_query($connection, $query);
		$content=array();
		$count=0;

		while ( $record=mysqli_fetch_assoc($result) ) 
		{
			// $content[$count]=$record["id"];
			// $count=$count+1;
			$content[$count]=$record["name"];
			$count=$count+1;
		}

		mysqli_free_result($result);
		return $content;
	}

	function get_gateway_info($gate_name, $table, $joined_table, $connection)
	{
		$id=get_id($gate_name, "gateways", $connection);
		$query="SELECT name ";
		$query.="FROM {$table} ";
		$query.="JOIN {$joined_table} ";
		$query.="ON {$table}.id={$joined_table}.option_id ";
		$query.="WHERE {$joined_table}.gate_id={$id} ";
		$result=mysqli_query($connection, $query);
		$count=0;
		$content=array();
		while ($row=mysqli_fetch_assoc($result) ) 
		{
			$content[$count]=$row["name"];
			$count=$count+1;
		}
		mysqli_free_result($result);
		return $content;
	}

	function get_rate_for_country($gate_name, $country_name, $type, $connection)
	{
		$id=get_id($gate_name, "gateways", $connection);
		$counrty_id=get_country_id($country_name, $connection);
		$query="SELECT {$type} ";
		$query.="FROM rates ";
		$query.="JOIN rates_gateways_countries ";
		$query.="ON rates.id=rates_gateways_countries.rate_id ";
		$query.="WHERE (rates_gateways_countries.gate_id={$id} AND rates_gateways_countries.country_id={$counrty_id} ) ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		mysqli_free_result($record);
		return $result["{$type}"];
	}

	function get_table_info($table, $connection)
	{
		$query="SELECT name ";
		$query.="FROM {$table} ";
		$records=mysqli_query($connection, $query);
		$count=0;
		$result=array();
		while ($row=mysqli_fetch_assoc($records) ) 
			{
				$result[$count]=$row["name"];
				$count=$count+1;
			}
		mysqli_free_result($records);
		return $result;
	}

	function get_id($name, $table, $connection)
	{
		$query="SELECT id ";
		$query.="FROM {$table} ";
		$query.="WHERE name='{$name}' ";
		$record=mysqli_query($connection, $query);
		$result=mysqli_fetch_assoc($record);
		if ($result["id"]) 
		{
			$id=$result["id"];
			mysqli_free_result($record);
			return $id;
		}
		else
		{
			mysqli_free_result($record);
			return 0;
		}
	}


	function get_name($id, $table, $connection)
	{
		$query="SELECT name ";
		$query.="FROM {$table} ";
		$query.="WHERE id='{$id}' ";
		$record=mysqli_query($connection, $query);
		$name=mysqli_fetch_assoc($record);
		return $name["name"];
	}
?>