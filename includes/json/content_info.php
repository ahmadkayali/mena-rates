<?php

	header("Content-Type: application/json");
	require('../db_connection.php');
	require('../functions.php');

	$name=$_GET["data"];
	$table=$_GET["table"];

	$id=get_id($name, $table, $connection);

	if ($table=="countries") 
	{
		$table="gate_country";
	}
	if ($table=="cards") 
	{
		$table="gate_card";
	}
	if ($table=="currencies") 
	{
		$table="gate_currency";
	}

	$query="SELECT name ";
	$query.="FROM gateways ";
	$query.="JOIN {$table} ";
	$query.="ON gateways.id={$table}.gate_id ";
	$query.="WHERE {$table}.option_id={$id}";

	$count=0;
	$records=mysqli_query($connection, $query);
	while ( $row=mysqli_fetch_assoc($records) ) 
	{
		$result[$count]=$row["name"];
		$count=$count+1;
	}

	mysqli_free_result($records);

	$output=array('name' => $name, 'gateways' => $result);
	$json_data=json_encode($output);
	echo $json_data;
	
?>