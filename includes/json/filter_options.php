<?php

	header("Content-Type: application/json");
	require('../db_connection.php');
	require('../functions.php');

	$countries=get_table_info("countries",$connection);
	$currencies=get_table_info("currencies",$connection);
	$cards=get_table_info("cards",$connection);
	$result=array('countries' => $countries , 'currencies' => $currencies , 'cards' => $cards);

	$json_data=json_encode($result);
	echo $json_data;

?>